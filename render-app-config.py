#!/usr/bin/env python

import sys
import os
import json
import argparse


def get_microflow_constants(metadata_file, constant_overrides):

    overrides = {}
    for o in constant_overrides:
        key = o.split("=")[0]
        overrides[key] = o.replace(f"{key}=", "")

    metadata = json.load(open(metadata_file, "r"))
    constants = metadata["Constants"]
    microflow_constants = {}
    for constant in constants:
        value = constant["DefaultValue"]
        name = constant["Name"]
        if constant["Name"] in overrides:
            value =  overrides[name]
        microflow_constants[name] = value
    return microflow_constants

def get_runtime_version(metadata_file):
    metadata = json.load(open(metadata_file, "r"))
    runtime_version = metadata["RuntimeVersion"]
    return runtime_version

def render_template(template_file, constants, runtime_version):
    template = json.load(open(template_file, "r"))
    template["spec"]["runtime"]["microflowConstants"] = constants
    template["spec"]["mendixRuntimeVersion"] = runtime_version
    return substitute(template)

def substitute(item):
    if isinstance(item, dict):
        for key, value in item.items():
            if isinstance(value, dict):
                item[key] = substitute(value)
            elif isinstance(value, list):
                item[key] = [substitute(i) for i in value]
            elif isinstance(value, str):
                item[key] = __substitute_value(value)
    elif isinstance(item, list):
        item = [substitute(i) for i in item]
    elif isinstance(item, str):
        item = __substitute_value(item)
    return item

def __substitute_value(item):
    for i in os.environ.keys():
        var_name = f"${i}"
        var_value = os.environ[i]
        if var_name in item:
            item = item.replace(var_name, var_value)
            break
    return item

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="Render a Mendix app CR for kubectl")
    parser.add_argument("-m", "--metadata", help="Path to metadata.json", default="metadata.json")
    parser.add_argument("-t", "--template", help="Path to app.json", default="app.json")
    parser.add_argument("-c", "--constant", help="Constant to set of KEY=VALUE format. Can be used multiple times. e.g. -c Module.Constant=mykey -c Module2.Hello=34", action="append", default=[])
    parser.add_help = True

    args = parser.parse_args()

    microflow_constants = get_microflow_constants(args.metadata, args.constant)
    runtime_version = get_runtime_version(args.metadata)
    result = render_template(args.template, microflow_constants, runtime_version)

    # output json for kubectl
    sys.stdout.write(json.dumps(result, indent=4))
